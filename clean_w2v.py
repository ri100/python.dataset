import re

from data_utils.encoders.dbs.word2vec_with_pos import word2vec_with_pos
r = r'^(((\d+[.,\/]\d+|\d+)\s*x\s*)?(\d+[.,\/]\d+|\d+)\s*[x-]\s*)?(\d+[.,\/]\d+|\d+)\s*(mb\/s|kb\/s|gb\/s|tb\/s|mbps|kbps|mbps|km\/s|m\/s|mph|proc|\%|sek|s|ms|ns|\μs|nanosek|milisek|min|h|godz|mmhg|pa|hpa|kpa|mbar|bar|kilo|kg|g|dg|dgram|dekagram|t|ton|lb|l|ml|hl|fl\.oz\.|fl\.oz|oz|F\,\ nF\,\ mF\,\ \μF|mb|kb|gb|tb|kohm|ohm|\Ω|\μ\Ω|k\Ω|\μhz|hz|khz|ghz|w|mw|kw|km|nm|n\/m|n|kn|mn|\μv|v|kv|mv|kwh|mwh|kj|\μa|ma|a|ka|ah|mah|kah|\°c|c|f|\°f|k|\°k|lm|lx|nx|mx|cd\·sr\/m2|cd|db|dbi|dbm|ha|ar|m2|mm2|cm2|dm2|km2|ha|m3|mm3|cm3|dm3|km3|mm|m|km|cm|inch|\"|in|p|i|px|pix|rpm|tdi|tdci|jtd|hdi|tsi|fsi)$'
r = re.compile(r, re.IGNORECASE | re.UNICODE)

for word, v in word2vec_with_pos.items():
    if r.match(word) or word.isnumeric():
        print(word)

    # if len(word.split('-'))>2:
    #     print(word)

    if '>' in word:
        print(word)
