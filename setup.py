from setuptools import setup


setup(
    name='data_utils',
    version='0.2',
    description='Utils for encoding and data set reading',
    author='Risto Kowaczewski',
    author_email='risto.kowaczewski@gmail.com',
    packages=['data_utils'],
    install_requires=[
        'annotator-enums @ git+https://ri100@bitbucket.org/ri100/python.annotator-enums.git#egg=annotator-enums'
        'numpy',
        'python-interface==1.5.3',
        'numba'],
    include_package_data=True
)
