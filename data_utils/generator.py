import types
from interface import implements

from data_utils.dataset import Dataset
from data_utils.encoder_list import EncoderList
from data_utils.encoders.encoder_interface import EncoderInterface


class Generator:

    def __init__(self, data, input_encoders: EncoderList, output_encoders: EncoderList):
        if not isinstance(data, types.GeneratorType):
            raise ValueError("Data param is not generator")
        if not isinstance(input_encoders, EncoderList):
            raise ValueError("Param input_encoders must be EncoderList")
        if not isinstance(output_encoders, EncoderList):
            raise ValueError("Param output_encoders must be EncoderList")
        self._data = data
        self._validate(input_encoders, 'input_encoders')
        self._validate(output_encoders, 'output_encoders')
        self._input_encoders = input_encoders
        self._output_encoders = output_encoders

    def __iter__(self):
        for x, y in self._data:
            x = self._input_encoders.encode(x)
            y = self._output_encoders.encode(y)
            yield tuple(x), tuple(y)

    def __call__(self, *args, **kwargs):
        return self.__iter__()

    @staticmethod
    def _validate(data, param_name):
        if not isinstance(data, dict):
            raise ValueError(f'Param {param_name} must be dict.')
        for name, encoder in data.items():
            if not isinstance(encoder, implements(EncoderInterface)):
                raise ValueError(
                    f'Encoder {param_name}.{name} must contain Encoders only that implement EncoderInterface.')

    def shapes(self):
        return self._input_encoders.shapes(), self._output_encoders.shapes()

    def types(self):
        return self._input_encoders.types(), self._output_encoders.types()

    def dataset(self):
        return Dataset(self)
