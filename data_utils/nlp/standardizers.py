import re
import html
from collections import defaultdict

from annotator_enums.enums import Key

_left_measures = {
    "fi": ['fi', 'ø'],  # fi 6
    "fokus": ['f'],  # f/10-20, f/2.8
    "memory_type": ['ddr', 'pc'],  # ddr3, ddr3-2300, pc2-1300
    'paper_size': ['a'],  # a4
    'group': ['gr.', 'gr'],
    'sun_protection': ['spf'],  # spf 50+
    'number': ['nr'],
    'cloth_size': ['roz', 'roz.'],  # roz. 10
}

_right_measures = {
    Key.PREDKOSC_DANYCH: ['mb/s', 'kb/s', 'gb/s', 'tb/s', 'mbps', 'kbps'],
    Key.PREDKOSC: ['mph', 'km/h', 'm/s', 'km/s'],
    Key.SPALANIE: ['l/km', 'l/h'],
    Key.UDZIAL: ['%', 'proc.'],
    Key.CZAS: ['sek.', 's', 'ms', 'ns', 'μs', 'nanosek', 'milisek', 'milisek.', 'milisekund', 'min.',
               'h', 'godz.'],
    Key.CISNIENIE: ['mmhg', 'pa', 'hpa', 'kpa', 'mbar', 'bar'],
    Key.WAGA: ['kilo', 'kg', 'gr', 'gr.', 'g', 'dg', 't', 'lb'],
    Key.POJEMNOSC: ['l', 'ml', 'hl', 'fl.oz.', 'fl.oz', 'oz', "F, nF, mF, μF"],
    Key.PAMIEC: ['mb', 'kb', 'gb', 'tb'],
    Key.OPORNOSC: ['kohm', 'ohm', 'Ω', 'μΩ', 'kΩ'],
    Key.JAKOSC_DRUKU: ['dpi', 'dpc'],
    Key.SREDNICA: ['ø', 'fi'],
    Key.CZESCTOTLIWOSC: ['μhz', 'hz', 'khz', 'ghz'],
    Key.MOC: ['w', 'mw', 'kw', 'wat', 'watt', 'kwh', 'mwh'],
    'horse_power': ['km'],
    Key.SILA: ['nm', 'n/m', 'n', 'kn', 'mn'],
    Key.NAPIECIE: ['μv', "v", 'kv', 'mv'],
    'current': ['μa', 'µa', 'ma', 'a', 'ka', 'ah', 'mah', 'kah'],
    Key.TEMPERATURA: ['c', 'f', 'k'],
    Key.KALORYCZNOSC: ['kcal', 'kj'],
    Key.LUMENY: ['lm', 'lx', 'nx', 'mx', 'cd·sr/m2', 'cd'],
    Key.DZWIEK: ['db', 'dbi', 'dbm'],
    Key.POWIERZCHNIA: ['mm2', 'cm2'],
    '3d_size': ['m3', 'mm3', 'cm3', 'dm3'],
    'field_size': ['ha', 'ar'],
    'room_size': ['m2', 'm.kw.'],
    Key.ROZMIAR: ['mm', 'm', 'dm', 'cm', '"', 'ft', 'mic', 'microna'],
    Key.ROZDZIELCZOSC: ['p', 'i', 'px', 'pix', 'mpix'],
    Key.RPM: ['rpm'],
    Key.POJEMNOSC_SILNIKA: ['tdi', 'tdci', 'jtd', 'hdi', 'tsi', 'fsi', 'vti', 'dci', 'dti', 'cdti', 'tid'],
    Key.CENA: ['pln', 'zł', '$'],
    Key.SZTUK: ['szt.', 'el.', 'kaps.', 'kpl.'],
    Key.ROCZNIK: ['r', 'rok', 'year', 'month', 'months', 'lat'],
    Key.ILOSC_PINOW: ['pin', 'pina', 'piny']
}

_right_measures_suffixes = [m for _, measures in _right_measures.items() for m in measures]
_right_measures_escaped = [re.escape(m) for m in _right_measures_suffixes]
_right_measure_group = "|".join(sorted(_right_measures_escaped, reverse=True, key=lambda x: len(x)))
_right_measure_reg = r'\b((?:(?:(\d{1,4}[.,\/]\d{1,3}|\d{1,4})\s*(x)\s*)?(\d{1,4}[.,\/]\d{1,3}|\d{1,4})\s*([x-])\s*)?(\d{1,4}[.,\/]\d{1,3}|\d{1,4})\s*(' + \
                     _right_measure_group + \
                     r'))(\s|$|\)|\])'

_left_measures_suffixes = [m for _, measures in _left_measures.items() for m in measures]
_left_measures_escaped = [re.escape(m) for m in _left_measures_suffixes]
_left_measure_group = "|".join(sorted(_left_measures_escaped, reverse=True, key=lambda x: len(x)))
_left_measure_reg = r'\b(' + _left_measure_group + r')\s*([-\/]*)\s*(\d+[\.,]\d+|\d+)(?:\s*(-)\s*(\d+))?\b'

# print(_left_measure_reg)
# print(_measure_right_side_reg)
_std_regexps = [
    (r"([@\/\\\"\'\?]{2,})", ""),
    (r'!{2,}', '!'),
    (r'~{2,}', ''),
    (r'_{2,}', ' '),
    (r'[,]{2,}', ','),
    (r'\.{2,}', '.'),
    (r'\b(\d+)\s+%\b', '\\1% '),  # procenty
    (r'\b(\d+|\d+[\.,]\d+)\s*(?:centymetrów|centymetra|centrymetr)\b', '\\1cm '),  # centrymetry
    (r'\b(\d+|\d+[\.,]\d+)\s*(?:decymetrów|decymetra|decymetr)\b', '\\1dm '),  # decymetry
    (r'\b(\d+|\d+[\.,]\d+)\s*(?:metrów|metra|metr)\b', '\\1m '),  # metry
    (r'\b(\d+)\s*(?:rok)\b', '\\1r '),  # rok
    (r'\b(\d+)\s*(?:dekagram|dgram)\b', '\\1dg '),  # dekagram
    (r'\b(\d+)\s*(?:tona|tony|ton)\b', '\\1t '),  # ton
    (r'\b(\d+)\s*(?:elementów|elementy|element|el)\b', '\\1el. '),  # elementy
    (r'\b(\d+)\s*(?:kologramów|kilograma|kilogram)\b', '\\1kg '),  # kilogram
    (r'\b(\d+)\s*(?:gramów|grama|gram)\b', '\\1g '),  # gram
    (r'\b(\d+)\s*(?:litrów|litra|litr)\b', '\\1l '),  # litra
    (r'\b(\d+)\s*(?:kompletów|komplety|komplet|kpl)\b', '\\1kpl. '),  # komplety
    (r'\b(\d+)\s*(?:procenta|procent|proc[\.]?)(?=$|\s|\))', '\\1%'),  # proc
    (r'\b(\d+)\s*(?:godzin|godz[\.]?)(?=$|\s|\))', '\\1h'),  # godzin
    (r'\b(\d+)\s*(?:minut|min)\b', '\\1min. '),  # minut
    (r'\b(\d+)\s*(?:sekund|sek)\b', '\\1sek. '),  # sekund
    (r'\b(\d+)\s*(?:cala|cali|inch|in)\b', '\\1\"'),  # cala
    (r'([^\s])\/([^\s])', '\\1 / \\2'),
    (r'([^\s])\/\s+', '\\1 / '),
    (r'\s+\/([^\s])', ' / \\1'),  # rozłącz /
    (r'\b(\d+[.,]\d+|\d+)\s*\/\s*(\d+[.,]\d+|\d+)\s*\/\s*(\d+[.,]\d+|\d+)\s*([cmd]?m)', '\\1/\\2/\\3/\\4'),
    # złącz 4 / 2.5 / 3.5cm
    (r'\b([a-z])\s/\s([a-z])\b', '\\1/\\2'),
    (r'\b(\d+[.,\/]\d+|\d+)\s*\/\s*(\d+[.,\/]\d+|\d+)\s*([k]?v)\b', '\\1/\\2\\3'),  # złącz  10 / 24v
    (r'(\s+|^)(\d+|\d+[,.]\d+)\s+\/\s+(\d+|\d+[,.]\d+)(\s+|$)', '\\1\\2/\\3\\4'),  # złącz 10 / 20

    (r"([A-zżźćńłóąśę+]+)([(\[])([A-zżźćńłóąśę+]+)", "\\1 \\2\\3"),  # nawiasy
    (r"([A-zżźćńłóąśę+]+)([)\]])([A-zżźćńłóąśę+]+)", "\\1\\2 \\3"),  # nawiasy

    (r'\b(\d+),(\d+)\b', '\\1.\\2'),
    (r'([\)\]])([^\s\-\.\,\;])', '\\1 \\2'),
    (r'([^\s-])([\(\[])', '\\1 \\2'),
    (r'\<!\s*\[\s*cdata\s*\[([^\]]+)(\]\s*\]\s*\>)?', '\\1'),
    (r'(\d)\'\'', '\\1"'),
    (r'(?<=[a-z\"\'\)\]\/])([.,;]+)([\"\'\(\[a-z0-9])', '\\1 \\2'),
    (r'(?<=[0-9\"\'\)\]\/])([.,;]+)([\"\'\(\[a-z])', '\\1 \\2'),
    (r'\s+([.,;|])\s+', '\\1 '),
    (r'(\w)([|+])(\w)', '\\1 \\2 \\3'),
    (r'\s+(\&)\s+', '\\1'),
    (r'\b(\d+)-\s+(\d+)\b', '\\1-\\2'),  # złacz 10- 10
    (r'\b(\d+)\s+-(\d+)\b', '\\1-\\2'),  # złacz 10 -10
    (r'\b(\d+)\s+-\s+(\d+)\b', '\\1-\\2'),  # złacz 10 - 10
    (r'[\.]{2,}', ''),
    (r' [\+]([a-z0-9])', ' + \\1'),
    (r' kpl.[,]? ', ' komplet '),
    (r'\brozm[.]* (\d{1,2}\b)', ' rozmiar \\1'),
    (r'\bd[lł][.]* (\d{1,4}\b)', ' długość \\1'),
    (r'\bwys[.]* (\d{1,4}\b)', ' wysokość \\1'),
    (r'\bszt[\.]? (\d+)\b', ' \\1 szt. '),  # sztuki
    (r'\b(\d+)\s*(szt[^\.]|sztuk[ai]?)\b', '\\1 szt. '),  # sztuki
    (r'\b\d+[-\/\\]([a-zęóąśłżźćń]{2,}(owa|owe|owy|ne|na|ny|ca|cy|ce))\b', ' #-\\1 '),  # #-pokojowa
    (r'(\d)(\s*|-|\s+-\s+)pok\.', '\\1 pokojowe '),
    (r'fl\s*[.]?\s*oz\s*[.]?', 'fl.oz.'),  # fl. oz.
    (r'\b(ddr)\s*([-]*)\s*([12345])(?:\s*(-)\s*(\d{3,4}))\b', '\\1\\3\\4\\5'),  # ddr4
    (r'\b(spf)\s*([-]*)\s*(\d{1,2})[+]?\b', '\\1\\3'),  #spf
    (r'\b(roz\.|gr\.|roz|fi|\ø|pc|gr|nr|f)\s*([-\/]*)\s*(\d+[\.,]\d+|\d+)(?:\s*(-)\s*(\d+))?\b', '\\1\\3\\4\\5'),

    (r'\b(ddr\d)(?:\s*(\-)\s*(\d{3,4}))*\b', "\\1\\2\\3"),  # ddr3, ddr3-3200
    (r'\b(pc[32]{0,1})\s*(\-)\s*(\d{3,5})\b', "\\1\\2\\3"),  # pc3-3200
    (r'\b(\d+)\s*[-]?\s*(pin[ya]?)\b', "\\1\\2"),  # 4-piny, 2 pin
    (r'\b(?:(\d+)\s*(x)\s*)?(GU10|MR16|MR11|G4|E14|E27|E40)\b', "\\1\\2\\3"),  # 2x MR16
    (r'\b4\s*x\s*4\b', '4x4'),
    (r'\b([ie][3579])\s*(\-)?\s*(\d{3,4}(?:[a-z]{1,3}[0-9]?|[a-z]{1,3}?)?)\b', '\\1-\\3'),  # rodzaj procesora
    (r'\b(dvd|cd)\s*([+-])\s*(ram|rw|r|w)\b', "\\1\\2\\3"),  # rodzaj płyty
    (r'\b(grupa|gr[\.]?)\s*([i]{1,3}|[12345])\b', 'gr.\\2'),  # grupa iii, grupa 4
    (r'\brj\s*(\d{2})\b', "rj-\\1"),  # rj-45
    (r'\b(cat)\s*([3-7][e]?|8(?:[\.][12])?)\b', '\\1\\2'),  # cat-3e
    (r'\bip[x]?\s*(\d(?:[a-dk][hmsw]?|[a-bk]?))\b', "ip\\1"),  # IPX6
    # rok 1988 r nr 1999 rok

    # rozmiar opon
    (
        r'\b(\d{2,3})\s*(\/)\s*(\d{2})\s*([a-z]*)\s*(r)\s*(\d+[,\.]\d+|\d{2,4})([a-z]?)[\s+_](?:(\d+\/\d+|\d{1,3})\s*([a-z]{1,3}))?\b',
        "\\1\\2\\3\\4\\5\\6\\7_\\8\\9 "),
    #
    (r'\b(\d+)\s*(km|m|mb|kb|gb|tb|l)\s+\/\s+(h|s|km)\b', '\\1\\2/\\3'),  # grupuj km / h
    (r'\b((?:(\d+[.,]\d+|\d+)\s*(\/)\s*)?(\d+[.,]\d+|\d+)\s*(inch|cali|cala|in|\"))(?=\s|$|\.|,)', "\\2\\3\\4\\5"),
    # grupuj cale 1/2"

    (r' {2,}', ' '),
]

_right_measures_regs_list = [
    (_right_measure_reg, "\\2\\3\\4\\5\\6\\7\\8"),
]
_right_measures_regs_list = [(re.compile(reg, re.IGNORECASE | re.UNICODE), string) for reg, string in _right_measures_regs_list]


_left_measure_regs_list = [
    (_left_measure_reg, "\\1\\3\\4\\5"),
]
_left_measure_regs_list = [(re.compile(reg, re.IGNORECASE | re.UNICODE), string) for reg, string in _left_measure_regs_list]

_serial_reg = r'^[a-z0-9\-\/\.]+$'
_number_reg = r'^(?:(\d+(?:[.,]\d+)?)?\s*[x]\s*)?(\d+(?:[.,]\d+)?)\s*x\s*(\d+(?:[.,]\d+)?)|' \
              r'(\d+(?:[.,]\d+)?\s*[-]\s*\d+(?:[.,]\d+)?)|' \
              r'(\d+[\/]\d+)|' \
              r'(\d+[.,]\d+|\d+)$'
_numbers_regs = [
    (_number_reg, "\\1"),
]
_serial_reg = re.compile(_serial_reg, re.IGNORECASE | re.UNICODE)
_number_reg = re.compile(_number_reg, re.IGNORECASE | re.UNICODE)
_numbers_regs = [(re.compile(reg, re.IGNORECASE | re.UNICODE), string) for reg, string in _numbers_regs]
_std_regexps = [(re.compile(reg, re.IGNORECASE | re.UNICODE), string) for reg, string in _std_regexps]

_right_measure2type = defaultdict(list)
for _m, ms in _right_measures.items():
    for m in ms:
        _right_measure2type[m].append(_m)

_left_measure2type = defaultdict(list)
for _m, ms in _left_measures.items():
    for m in ms:
        _left_measure2type[m].append(_m)


class StandardizedText(str):

    def __new__(cls, content, aux_measures=None):
        string = str.__new__(cls, content)
        string.aux_measures = aux_measures if aux_measures is not None else {}
        return string

    def _only_letters(self, token):
        match = re.match(r"^[a-zęóąśłżźćń\-'&]+$", token)
        return match is not None and (token[0] != '-' and token[0] != "'")

    @staticmethod
    def _is_tag(token):
        return token and token[0] == "<" and token[-1] == ">" and token.upper() == token

    def _find_right_measures_and_standarize(self, text):
        aux_measures = {}
        # miary
        for regex, string in _right_measures_regs_list:
            for m in regex.finditer(text):
                _measure_abrv = m.expand("\\7")
                if _measure_abrv in _right_measure2type:
                    _measure_value = m.expand("\\2\\3\\4\\5\\6\\7")
                    _, value3, _, value2, _, value1, _, _ = m.groups()
                    aux_measures[_measure_value] = value1, value2, value3, _measure_abrv, _right_measure2type[_measure_abrv]
            text = regex.sub(string, text)
        return text, aux_measures

    def _find_left_measures_and_standarize(self, text):
        aux_measures = {}
        # miary
        for regex, string in _left_measure_regs_list:
            for m in regex.finditer(text):
                _measure_abrv = m.expand("\\1")
                if _measure_abrv in _left_measure2type:
                    _measure_value = m.expand("\\1\\3\\4\\5")
                    _, _, value1, _, value2 = m.groups()
                    aux_measures[_measure_value] = value1, value2, None, _measure_abrv, _left_measure2type[_measure_abrv]
            text = regex.sub(string, text)
        return text, aux_measures

    def standardize(self):

        text = self.replace('\n', ' ')
        text = text.replace('\r', ' ')
        text = text.replace('_', ' ')
        text = text.replace('`', '\'')
        text = text.replace("''", '"')
        text = text.replace('…', '')
        text = text.replace('½', '1/2')
        text = text.replace('✔', '')
        text = text.replace('✄', '')
        text = text.replace('²', '2')
        text = text.replace('³', '3')
        text = text.replace('°', '')
        text = html.unescape(text)
        text = html.unescape(text)

        for regex, string in _std_regexps:
            text = regex.sub(string, text)

        # text, aux_measures = self._find_left_measures_and_standarize(text)
        text, aux_measures = self._find_right_measures_and_standarize(text)

        return StandardizedText(text, aux_measures)

    def split_with_measures(self):
        for token in self.tokenize(True):
            measure_type = self.aux_measures[token] if token in self.aux_measures else None
            yield token, measure_type

    def split(self, sep=None, maxsplit=-1):
        return self.tokenize(True)

    def tokenize(self, trim_punctuation=False, digits_as_tags=False, tag_non_alpha=False):
        tokens = str(self).split()
        punctuation = "!#-&*,./:;=?@\^_`|~"  # samotnie nie mogą występować
        l_trim = "!<([{=/"
        r_trim = "!$?>)]}=/"
        if trim_punctuation:
            for token in tokens:

                if token in punctuation:
                    continue
                elif token.strip(punctuation) != '':
                    if not self._is_tag(token):
                        token = token.lstrip(l_trim)
                        token = token.rstrip(r_trim)

                if digits_as_tags and token.isdigit():
                    token = '<CYFRA>'
                elif tag_non_alpha and not self._is_tag(token) and not self._only_letters(token):
                    token = '<UKN>'

                yield token
        else:
            for token in tokens:
                yield token

    def lower(self) -> 'StandardizedText':
        return StandardizedText(super().lower())


if __name__ == "__main__":
    t = 'aasd fi 600mm gr.4 pc 3200- 2324 as f/10.5 - 20, f/ 2.8 ddr 3, ddr3 -2300, pc2 - 1300 roz. 10 23 m³ 1.0min 2 godz 10 in 1cala 10el (10 proc.) 2.50ghz (62cm) 2/3xl 2x 205 / 55 R16 91H 33299 -2szt 8 ala 1szt. 4-pin  (5 mpix) 3 x 123.5mb /s 28 km /h 24 h 2-pojokowy (2.75/3-12g) 1.5 metra 10 centymetrów 2elementy 2019 rok + 1/ 14" d19wau-20 1 / 2 1190,45 -  1291.5 (3 komplety) 1 / 4"'

    # t = "23 m³ nity miedziane łeb 1.5litra kulisty 100 km - fi 5/16 mm - 200 szt 123123c"
    a = StandardizedText(t)
    # print(list(a.lower().tokenize(trim_punctuation=True)))
    std = a.lower().standardize()
    print(std)
    print(std.aux_measures)
    tk = std.split_with_measures()
    print(list(tk))
