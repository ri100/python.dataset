from interface import implements

from data_utils.encoders.dbs.word2vec import word2vec
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros


class NotWord2VecMaskEncoder(implements(EncoderInterface)):

    def __init__(self, max_words):
        self.max_words = max_words
        self.word2vec = word2vec
        self._shape = (self.max_words,)
        self._type = "float32"
        self._dim = ()

    def encode(self, data):
        vector = zeros(self.max_words)
        for p, token in enumerate(data.split()[:self.max_words]):
            if token not in self.word2vec:
                vector[p] = 1.0
        return vector

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    w = NotWord2VecMaskEncoder(16)
    e = w.encode("kajdanki erotyczne niebieskie sfdgsd")
    print(e)
    print(e.shape)
