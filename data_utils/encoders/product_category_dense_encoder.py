import numpy as np
from data_utils.encoders.product_category_sparse_encoder import ProductCategorySparseEncoder


class ProductCategoryDenseEncoder(ProductCategorySparseEncoder):

    def __init__(self, level):
        super().__init__(level)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, list):
            raise ValueError("Param data must be a list of int values. {} given fo type {}".format(data, type(data)))

        """
        Data  is a list of category ids on each level,
        e.g. [1, 24, 65]
        Return dense one hot encoded vector
        """
        vector = np.zeros(self._shape)
        category_id = self._get_category(data)
        vector[category_id] = 1.0
        return vector


if __name__ == "__main__":
    wae = ProductCategoryDenseEncoder(level=2)
    a = wae.encode([24])
    print(a)
    print(a.shape)
    print(wae.decode(a))
