import numpy as np
from interface import implements

from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.word2char_encoder import Word2CharEncoder
from data_utils.encoders.dbs.word2vec import word2vec


class NotWord2VecCharEncoder(implements(EncoderInterface)):

    def __init__(self, slots, max_chars, max_words):
        self.slots = slots
        self._encoder = Word2CharEncoder(max_chars)
        self.max_words = max_words
        self.max_chars = max_chars
        self.word2vec = word2vec
        self._shape = (self.slots, self.max_chars, self._encoder.dim())
        self._type = "float32"
        self._dim = self._encoder.dim()

    def encode(self, data):
        tensor = np.zeros(self._shape)
        i = 0
        for token in data.split()[:self.max_words]:
            if token not in self.word2vec:
                if i >= self.slots:
                    break
                tensor[i] = self._encoder.encode(token)
                i += 1

        return np.array(tensor)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    w = NotWord2VecCharEncoder(13, 5, 15)
    e = w.encode("kajdanki 1aaa 1bbb 1ccc")
    print(e)
    print(e.shape)
