from data_utils.encoders.encoder_interface import EncoderInterface
from interface import implements
from data_utils.math.ops import zeros
import numpy as np


class CharEncoder(implements(EncoderInterface)):

    vector_space = dict(
        chars='abcdefghijklmnopqrstuwvxyzπœę©ßóþąśðæŋəłżźćńµ„”`1234567890-=~!@#$%^&*()[]{}_+;\':\",./<>?\|"×',
        numbers='1234567890,.',
        alpha='abcdefghijklmnopqrstuwvxyzπœęßóþąśðæŋəłżźćńµ',
        non_polish='πœßþðæŋəµ',
        endings='?!.',
        separators='+/|-\\&;:',
        brackets='{}[]()<>',
        quotes="\'\"„”`",
        multi="×x",
        other="~!@#$%^&*_"
    )

    feature_position = dict(
        numbers=0,
        alpha=1,
        non_polish=2,
        endings=3,
        separators=4,
        brackets=5,
        quotes=6,
        multi=7,
        other=8
    )

    def __init__(self, max_chars_in_word, flatten=False, reverse=False):
        self.reverse = reverse
        self.flatten = flatten
        self.max_chars_in_word = max_chars_in_word
        self._dim = len(self.feature_position)
        self._char_dim = len(self.vector_space['chars'])
        self._shape = (max_chars_in_word, self._char_dim, self._dim)
        self._type = "float32"
        self._char2idx = {char: idx for idx, char in enumerate(list(self.vector_space['chars']))}

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Param data must be string. {} given fo type {}".format(data, type(data)))

        if self.reverse:
            data = data[::-1]

        vector = zeros(self._shape)
        for char_position, char in enumerate(list(data)[-self.max_chars_in_word:]):
            # encode chars
            char_feature_vector = zeros(self._dim)
            for feature, pos in self.feature_position.items():
                if char in self.vector_space[feature]:
                    char_feature_vector[pos] = 1.0  # chars

            if char in self._char2idx:
                char_index = self._char2idx[char]

                vector[char_position][char_index] = char_feature_vector

        if self.flatten:
            vector = np.reshape(vector, (self.max_chars_in_word, self._char_dim*self._dim))
        return vector

    def shape(self):
        if self.flatten:
            return self.max_chars_in_word, self._char_dim*self._dim
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = CharEncoder(10, flatten=True)
    a = wae.encode('300cm')
    print(a.shape)
    for i,x in enumerate(a):
        print(x)
        # if x.all() == 0:
        #     print(i)
