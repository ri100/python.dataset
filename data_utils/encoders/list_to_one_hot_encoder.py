from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros
import numpy as np


class ListToOneHotEncoder(implements(EncoderInterface)):

    def __init__(self, category_list):
        self.category_list = category_list
        self._dim = len(category_list)
        self._shape = (self._dim, )
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Data must be string. {} {} given".format(type(data), data))

        vector = zeros(self._dim)
        if data in self.category_list:
            idx = self.category_list.index(data)
            vector[idx] = 1.0
        return vector

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    from annotator_enums.enums import Key
    w = ListToOneHotEncoder(["abc", "dfg"])
    e = w.encode('dfg_')
    print(e)
    print(np.argmax(e))
    print(e.shape)



