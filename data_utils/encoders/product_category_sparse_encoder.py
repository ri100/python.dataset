import numpy as np
from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface


class ProductCategorySparseEncoder(implements(EncoderInterface)):

    def __init__(self, level):
        self.level = level
        self.category_level_dims = [13,
                                    94 + 13,
                                    1103 + 94 + 13,
                                    6130 + 1103 + 94 + 13,
                                    11148 + 6130 + 1103 + 94 + 13,
                                    5297 + 11148 + 6130 + 1103 + 94 + 13,
                                    873 + 5297 + 11148 + 6130 + 1103 + 94 + 13,
                                    14 + 873 + 5297 + 11148 + 6130 + 1103 + 94 + 13,
                                    3 + 14 + 873 + 5297 + 11148 + 6130 + 1103 + 94 + 13]
        self._dim = self._get_dim()
        self._shape = (self._dim, )
        self._type = "int64"

    def _get_dim(self):
        return self.category_level_dims[self.level]

    def _get_category(self, data):
        if len(data) <= self.level:
            return data[-1]
        return data[self.level]

    def encode(self, data):
        """
        Data  is a list of category ids on each level,
        e.g. [1, 24, 65]
        Return sparse category_id
        """
        return self._get_category(data)

    def decode(self, tensor):
        return np.argmax(tensor)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = ProductCategorySparseEncoder(level=5)
    a = wae.encode([1, 24, 65])
    print(a)
