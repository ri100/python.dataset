from data_utils.encoders.encoder_interface import EncoderInterface
from interface import implements
from data_utils.math.ops import zeros
from data_utils.encoders.dbs.word2vec import word2vec
import numpy as np


class VerboseCharEncoder(implements(EncoderInterface)):

    vector_space = dict(
        chars='abcdefghijklmnopqrstuwvxyzπœę©ßóþąśðæŋəłżźćńµ„”`1234567890-=~!@#$%^&*()[]{}_+;\':\",./<>?\|"×',
        numbers='1234567890,.',
        alpha='abcdefghijklmnopqrstuwvxyzπœęßóþąśðæŋəłżźćńµ',
        non_polish='πœßþðæŋəµ',
        endings='?!.',
        separators='+/|-\\&;:',
        brackets='{}[]()<>',
        quotes="\'\"„”`",
        multi="×x",
        other="~!@#$%^&*_"
    )

    feature_position = dict(
        numbers=0,
        alpha=1,
        non_polish=2,
        endings=3,
        separators=4,
        brackets=5,
        quotes=6,
        multi=7,
        other=8
    )

    def __init__(self, max_words, max_chars_in_word, skip_valid_word2vec=True, flatten=False, reverse=False):
        self.reverse = reverse
        self.flatten = flatten
        self.max_words = max_words
        self.max_chars_in_word = max_chars_in_word
        self._dim = len(self.feature_position)
        self._char_dim = len(self.vector_space['chars'])
        self._shape = (max_words, max_chars_in_word, self._char_dim, self._dim)
        if skip_valid_word2vec:
            self.word2vec = word2vec
        else:
            self.word2vec = None
        self._type = "float32"
        self._char2idx = {char: idx for idx, char in enumerate(list(self.vector_space['chars']))}

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Param data must be string. {} given fo type {}".format(data, type(data)))

        if self.reverse:
            data = data[::-1]
        vector = zeros(self._shape)
        for token_position, token in enumerate(data.split()[:self.max_words]):
            if not self.word2vec or token not in self.word2vec:
                for char_position, char in enumerate(list(token)[-self.max_chars_in_word:]):
                    # encode chars
                    char_feature_vector = zeros(self._dim)
                    for feature, pos in self.feature_position.items():
                        if char in self.vector_space[feature]:
                            char_feature_vector[pos] = 1.0  # chars

                    if char in self._char2idx:
                        char_index = self._char2idx[char]

                        vector[token_position][char_position][char_index] = char_feature_vector

        if self.flatten:
            vector = np.reshape(vector, (self.max_words, self.max_chars_in_word, self._char_dim*self._dim))
            # vector = np.expand_dims(vector, axis=-1)
        return vector

    def shape(self):
        if self.flatten:
            return self.max_words, self.max_chars_in_word, self._char_dim*self._dim
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = VerboseCharEncoder(16, 10, skip_valid_word2vec=False, flatten=True)
    a = wae.encode('300cm ma ßał-ł9."/%$ł')
    print(a.shape)
    for i,x in enumerate(a):
        print(np.sum(x))
        # if x.all() == 0:
        #     print(i)
