from interface import implements

from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_3d
from data_utils.math.ops import zeros


class Word2CharEncoder(implements(EncoderInterface)):

    def __init__(self, max_chars, append_len=False):
        self.max_chars = max_chars
        self._append_len = append_len
        self._available_chars = " abcdefghijklmnopqrtsuvwxyz1234567890-+=ąłśęóżźćń.,/':;&%$\"()_"
        self._char2index = {c: i for i, c in enumerate(self._available_chars)}
        self._dim = len(self._available_chars)
        self._shape = (self.max_chars, self._dim)
        self._type = "float32"

    def _one_hot(self, index):
        vector = zeros(self._dim)
        vector[index] = 1.0
        return vector

    def encode(self, data):
        result = []
        for char in data:
            if char in self._char2index:
                oh = self._one_hot(self._char2index[char])
                result.append(oh)

        if self._append_len:
            return pad_3d(result, self.max_chars, self._dim), len(data)
        return pad_3d(result, self.max_chars, self._dim)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self._char2index


if __name__ == "__main__":
    from tqdm import tqdm
    ch = Word2CharEncoder(16, append_len=False)
    for _ in tqdm(range(1000000)):
        print(ch.encode("asksaldlsakdlkas ldlsakdlsakldksad salkdjasd").shape)
