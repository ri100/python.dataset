from data_utils.encoders.encoder_interface import EncoderInterface
from interface import implements
import numpy as np


class BitEncoder(implements(EncoderInterface)):

    def __init__(self, threshold=.5):
        self.threshold = threshold
        self._dim = 1
        self._shape = (1,)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, float) and not isinstance(data, int):
            raise ValueError("Param data must be float or int. {} given fo type {}".format(data, type(data)))

        # if 0 <= data <= 1:
        #     raise ValueError("Param data must be either 0 or 1. {} given".format(data))
        value = 1 if data>self.threshold else 0
        vector = np.array([value])

        return vector

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = BitEncoder()
    a = wae.encode(.51)
    print(a)
    print(a.shape)
