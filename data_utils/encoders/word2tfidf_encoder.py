from interface import implements
from data_utils.encoders.dbs.word2tfidf import word2tfidf, word2tfidf_dim
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros


class Word2TfidfEncoder(implements(EncoderInterface)):

    def __init__(self, max_words, skip_empty=False):
        self.skip_empty = skip_empty
        self.max_words = max_words
        self.word2tfidf = word2tfidf
        self._dim = word2tfidf_dim
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def encode(self, data):
        result = []
        for token in data.split()[:self.max_words]:
            if token in self.word2tfidf:
                result.append(self.word2tfidf[token])
            elif not self.skip_empty:
                result.append(zeros(self._dim))

        try:
            return pad_2d(result, self.max_words)
        except ValueError:
            result = zeros((self.max_words, self._dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2tfidf


if __name__ == "__main__":
    import numpy as np
    w = Word2TfidfEncoder(16)
    e = w.encode("kajdanki erotyczne niebieskie")
    print(np.sum(e[0]))
    print(np.sum(e[1]))
    print(np.sum(e[2]))
    print(np.sum(e[3]))
    print(e.shape)
