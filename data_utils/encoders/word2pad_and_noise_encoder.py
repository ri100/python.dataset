from collections import defaultdict
import numpy as np
from annotator_nlp.preprocessing import standardize, tokenize_sentence
from interface import implements

from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_3d
from data_utils.math.ops import zeros


class Word2PadAndNoiseEncoder(implements(EncoderInterface)):

    def __init__(self, max_words):
        self.max_words = max_words
        self._dim = 1
        self._shape = (self.max_words, )
        self._type = "float32"

    @staticmethod
    def x_in_y(query, base):
        try:
            l = len(query)
        except TypeError:
            l = 1
            query = type(base)((query,))

        for i in range(len(base)):
            if base[i:i + l] == query:
                return True, (i, i + l)
        return False, (0, 0)

    def encode(self, data):
        title, tags = data
        title_words_no_punc = list(tokenize_sentence(title, True))
        title_words_punc = list(tokenize_sentence(title, False))
        vector = np.array([0.,0., 1.])  # pad
        vector = np.repeat(np.array([vector]), self.max_words, axis=0)
        vector[0: len(title_words_punc)] = [0., 1., 0.]  # word

        if 'noise' in tags:
            noises = tags['noise']
            for noise in noises:
                has_noise, (where_start, where_stop) = self.x_in_y(noise.split(), title_words_no_punc)
                if has_noise:
                    vector[where_start: where_stop] = [1., 0., 0.]  # noise

                has_noise, (where_start, where_stop) = self.x_in_y(noise.split(), title_words_punc)
                if has_noise:
                    vector[where_start: where_stop] = [1., 0., 0.]  # noise

        return vector

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = Word2PadAndNoiseEncoder(16)
    title = "odkurzacz zwrot 23% bosch bgl3a234  ceny na konto"
    words = title.split()
    a = wae.encode(
        (title,
         {"serial": ["1askkda"],
          "pieces": ["100 sztuk"],
          "size": ["21mm"],
          "products": ["kapsle", "moneta", "szybka"],
          "na": ["monety"],
          "noise": [" zwrot 23%"," ceny na konto"]
          })
    )
    print(a.shape)
    print(a)
