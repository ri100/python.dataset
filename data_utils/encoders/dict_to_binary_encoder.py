import math
from collections import defaultdict

from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros
import numpy as np


class DictToBinaryEncoder(implements(EncoderInterface)):

    def __init__(self, value2idx_dict, post_process=None, normalize=False):

        self.post_process = post_process
        self.normalize = normalize
        if '<none>' not in value2idx_dict:
            value2idx_dict['<none>'] = max(value2idx_dict.values()) + 1

        self.value2idx = value2idx_dict
        self.idx2value = defaultdict(list)
        for value, idx in self.value2idx.items():
            self.idx2value[idx].append(value)
        self._dim = max(self.idx2value.keys()) + 1
        self._shape = (self._dim,)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, list):
            raise ValueError("Data must be list. {} {} given".format(type(data), data))

        vector = zeros(self._dim)
        boosted_vector = zeros(self._dim)
        boosted_vector_base = zeros(self._dim)
        is_empty = True
        for item in data:
            if item in self.value2idx:
                idx = self.value2idx[item]
                vector[idx] = 1.0

                if self.post_process:
                    aux = self.post_process.process_item(item)
                    base = math.log(aux, 2) if aux != 0 else 0
                    boosted_vector[idx] = 1 / base if base != 0 else 1
                    boosted_vector_base[idx] = 1

                is_empty = False

        if is_empty:
            idx = self.value2idx['<none>']
            vector[idx] = 1.0
        elif self.post_process:
            vector = self.post_process.process_vector(boosted_vector, boosted_vector_base)
        elif self.normalize:
            vector = vector / np.sum(vector)

        return vector

    def decode(self, vector, threshold=.5):
        if not isinstance(vector, np.ndarray):
            raise ValueError("Decoded data must be list. {} {} given".format(type(vector), vector))

        for p, v in enumerate(vector):
            if v >= threshold:
                yield self.idx2value[p], vector[p]

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    from annotator_enums.enums import Key
    from data_utils.utils.serializer import unserialize


    # labels_counts = {
    #     "context": {
    #         "dziecko": 10,
    #         "motoryzacja": 20,
    #     },
    #     "set": {
    #         "motoryzacja": 5,
    #     }
    # }

    class BinaryVecProcessor:

        def __init__(self):
            self.labels_counts = unserialize(
                "/home/risto/PycharmProject/python.annotator-final/pickles/dataset-26/labels_countes.pkl")
            # self.labels_counts = {
            #     "context": {
            #         "dziecko": 10000,
            #         "motoryzacja": 2000000,
            #     },
            #     "set": {
            #         "motoryzacja": 200,
            #     }
            # }

        def process_item(self, item):
            key, value = item.split("|")
            if key not in self.labels_counts:
                return 0
            return self.labels_counts[key][value] if value in self.labels_counts[key] else 0

        def process_vector(self, boosted_vector, vector):
            boosted_vector = boosted_vector / np.max(boosted_vector)
            vector = vector + boosted_vector
            return vector / np.max(vector)


    w = DictToBinaryEncoder({"context|części": 0, "context|motoryzacja": 1, "related|do drzwi samochodowych": 2},
                            post_process=BinaryVecProcessor())
    e = w.encode(["context|części", "context|motoryzacja","related|do drzwi samochodowych"])
    # wyświetl znane ci cechy
    print(e)
    print(list(w.decode(e)))
