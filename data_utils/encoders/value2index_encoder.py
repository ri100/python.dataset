from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros
import numpy as np


class Value2IndexEncoder(implements(EncoderInterface)):

    def __init__(self, value2index, max_words, dim=1, skip_empty=False, one_hot=False, add_pad_class=True):

        self.add_pad_class = add_pad_class
        if not isinstance(value2index, dict):
            raise ValueError("Value2IndexEncoder requires value2index to be dict.")

        self.max_words = max_words
        self.one_hot = one_hot
        self.skip_empty = skip_empty
        self.value2index = value2index
        self._value2index_dim = len(self.value2index) + 2 if one_hot else dim
        self._shape = (self.max_words, self._value2index_dim)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Value2IndexEncoder requires data to be string.")
        result = []
        for token in data.split()[:self.max_words]:

            if token not in self.value2index and self.skip_empty:
                continue

            if token in self.value2index:
                value = self.value2index[token]
            else:
                value = None

            if self.one_hot:
                vector = zeros(self._value2index_dim)
                if value:
                    vector[value] = 1.0
                value = vector

            result.append(value)

        try:
            if self.one_hot:
                return pad_2d(result, self.max_words, self.add_pad_class)
            return np.pad(result, (0, self.max_words-len(result)), mode='constant')
        except ValueError:
            result = zeros((self.max_words, self._value2index_dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._value2index_dim

    def __contains__(self, token):
        return token in self.value2index


if __name__ == "__main__":
    d = {
        "ala": 0,
        "ma": 1,
        "kota": 2,
    }
    w = Value2IndexEncoder(d, 15, one_hot=True, skip_empty=True, add_pad_class=False)
    e = w.encode("ala ma kota ale")
    print(e)
    print(e.shape)
