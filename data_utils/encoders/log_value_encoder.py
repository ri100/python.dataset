import numpy as np
import math
from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface


class LogValueEncoder(implements(EncoderInterface)):

    def __init__(self, base=10, dtype="float32"):
        self.base = base
        self._dim = 1
        self._shape = (self._dim, )
        self._type = dtype

    def encode(self, data):
        return np.array(math.log(data, self.base), dtype=self._type)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    import numpy as np
    w = LogValueEncoder(dtype="float32")
    e = w.encode(1001)
    print(w.shape())
    print(e)

