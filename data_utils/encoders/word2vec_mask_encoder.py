import numpy as np
from interface import implements
from data_utils.encoders.dbs.word2vec import word2vec
from data_utils.encoders.encoder_interface import EncoderInterface


class Word2VecMaskEncoder(implements(EncoderInterface)):

    def __init__(self, max_words, reverse=False):
        self.reverse = reverse
        self.max_words = max_words
        self.word2vec = word2vec
        self._dim = 1
        self._shape = (self.max_words, )
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Word2VecMaskEncoder requires data to be string.")
        result = []
        for token in data.split()[:self.max_words]:
            if token in self.word2vec:
                t = 1.0 if not self.reverse else 0.
                result.append(t)
            else:
                t = 0.0 if not self.reverse else 1.
                result.append(t)

        return np.pad(result, (0, self.max_words-len(result)), mode='constant')

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    w = Word2VecMaskEncoder(16, reverse=True)
    e = w.encode("kajdanki 234edwe erotyczne niebieskie")
    print(e)
    print(e.shape)
    print(w.shape())
