import numpy as np
from collections import defaultdict
from interface import implements
from data_utils.encoders.dbs.word2vec import word2vec_dim, word2vec
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros


class Word2TagGraphEncoder(implements(EncoderInterface)):

    def __init__(self):
        self.tags = [
            'brand',
            'products',
            'creation',
            '=+',
            '_',
            'bez',
            'dla',
            'do',
            'na',
            'nad',
            'od',
            'po',
            'pod',
            'przeciw',
            'przed',
            'przez',
            'w',
            'z',
            'za',
            'ze',
        ]
        self.word2vec = word2vec
        self.tag2idx = {v: k for k, v in enumerate(self.tags)}
        self._dim = word2vec_dim
        self._shape = (len(self.tags), self._dim)
        self._type = "float32"

    def _tag2words(self, tags):
        tag2words = defaultdict(list)
        for tag_type, tag_words in tags.items():
            for tag_chunk in tag_words:
                tag_words = tag_chunk.split()
                for tag_word in tag_words:
                    if tag_word in self.word2vec:
                        tag2words[tag_type].append(self.word2vec[tag_word])
        return tag2words

    def encode(self, data):
        tag2words = self._tag2words(data)
        result = []
        for tag in self.tags:
            if tag in tag2words:
                tag_vec = np.sum(np.array(tag2words[tag]), axis=-2)
            else:
                tag_vec = zeros(self._dim)
            result.append(tag_vec)
        return np.array(result)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = Word2TagGraphEncoder()
    print(wae.shape())
    a = wae.encode(
         {"w": ["kratę"],
          "do": ["zębów"],
          "size": ["21mm"],
          "products": ["kapsle"],
          "na": ["monety"]}
    )
    for i, x in enumerate(a):
        print(wae.tags[i], np.sum(x))
