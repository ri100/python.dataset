import numpy as np
from interface import implements

from data_utils.encoders.dbs.word2pos import word2pos, word2pos_dim
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros
from data_utils.utils.sparse_2_dense import SparseToDense


class PosEncoder(implements(EncoderInterface)):

    def __init__(self, max_words):
        self.max_words = max_words
        self.word2pos = SparseToDense(word2pos, dim=word2pos_dim + 2)  # 2 wiecej dla nieznany i pad
        self._dim = self.word2pos.dim
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def encode(self, data):
        poses = []
        if not isinstance(data, str):
            raise ValueError("PosEncoder requires data to be string.")

        tokens = data.split()
        tokens = tokens[:self.max_words]

        for position, token in enumerate(tokens):

            # part of speech
            if token in self.word2pos.data:
                pos_vec = self.word2pos[token]
            else:
                # nieznany
                pos_vec = zeros(self._dim)
                pos_vec[-2] = 1.0

            poses.append(pos_vec)

        poses = pad_2d(poses, self.max_words)

        return poses

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    enc = PosEncoder(16)
    text = "czerwona męska koszula w kratę kratką z krótkim rękawem zawias i podpinka"
    text = "koszulka dla chłopca rozmiar xxl bawełna"
    text = "60CM ODPŁYW M04 NISKI ODPŁYW LINIOWY POD PŁYTKĘ KOMPLET".lower()
    text = "chusteczki do czyszczenia g\u0142owic 150szt"

    pos = enc.encode(text)
    print(np.sum(pos[0]))
    print(pos.shape)
