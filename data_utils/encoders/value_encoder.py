import numpy as np
from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface


class ValueEncoder(implements(EncoderInterface)):

    def __init__(self, dtype):
        self._dim = 1
        self._shape = (self._dim, )
        self._type = dtype

    def encode(self, data):
        return np.array(data, dtype=self._type)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    import numpy as np
    w = ValueEncoder()
    e = w.encode(1.4)
    print(w.shape())
    print(e)

