import numpy as np
from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface


class ProductCategorySparseAutoLevelEncoder(implements(EncoderInterface)):

    def __init__(self):
        self._dim = 873 + 5297 + 11148 + 6130 + 1103 + 94 + 13
        self._shape = (self._dim, )
        self._type = "int64"

    def _get_category(self, data, level):
        if len(data) <= level:
            return data[-1]
        return data[level]

    def encode(self, data):
        """
        Data  is a list of category ids on each level,
        e.g. [1, 24, 65], level
        Return sparse category_id
        """
        path, level = data
        return self._get_category(path, level)

    def decode(self, tensor):
        return np.argmax(tensor)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    wae = ProductCategorySparseAutoLevelEncoder()
    a = wae.encode(([1, 24, 65], 1))
    print(a)
