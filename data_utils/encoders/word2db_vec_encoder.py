from interface import implements

from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros
import numpy as np

# duplicate for word2vec for dict
class Word2DbVecEncoder(implements(EncoderInterface)):

    def __init__(self, max_words, db, db_dim, skip_empty=False):
        self.skip_empty = skip_empty
        self.max_words = max_words
        self.db = db
        self._dim = db_dim
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("WOrd2DbVecEncoder requires data to be string.")
        result = []
        for token in data.split()[:self.max_words]:
            if token in self.db:
                result.append(self.db[token])
            elif not self.skip_empty:
                result.append(np.zeros(self._dim))

        try:
            return pad_2d(result, self.max_words)
        except ValueError:
            result = zeros((self.max_words, self._dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.db


if __name__ == "__main__":
    from data_utils.encoders.dbs.word2pig import word2pig, word2pig_dim
    w = Word2DbVecEncoder(16, word2pig, db_dim=word2pig_dim)
    e = w.encode("kajdanki erotyczne niebieskie")
    print(e)
    print(e.shape)
