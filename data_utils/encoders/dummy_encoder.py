from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface


class DummyEncoder(implements(EncoderInterface)):

    def __init__(self, dim, debug=False):
        self.debug = debug
        self._dim = dim
        self._shape = (self._dim, )
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, list):
            raise ValueError("Data must be list.")
        if self.debug:
            print(data)
        return np.array(data)

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2tfidf


if __name__ == "__main__":
    import numpy as np
    w = DummyEncoder(3)
    e = w.encode([1,2,3])
    print(e.shape)
    print(e)

