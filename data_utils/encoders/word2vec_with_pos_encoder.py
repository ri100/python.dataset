import numpy as np
from interface import implements
from data_utils.encoders.dbs.word2vec_with_pos import word2vec_with_pos, word2vec_with_pos_dim
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros


class Word2VecWithPosEncoder(implements(EncoderInterface)):

    def __init__(self, max_words, skip_empty=False):
        self.skip_empty = skip_empty
        self.max_words = max_words
        self.word2vec = word2vec_with_pos
        self._dim = word2vec_with_pos_dim
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("WOrd2VecWithPosEncoder requires data to be string.")
        result = []
        for token in data.split()[:self.max_words]:
            if token in self.word2vec:
                result.append(self.word2vec[token])
            elif not self.skip_empty:
                result.append(np.zeros(self._dim))

        try:
            return pad_2d(result, self.max_words)
        except ValueError:
            result = zeros((self.max_words, self._dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    w = Word2VecWithPosEncoder(16)
    e = w.encode("kajdanki erotyczne niebieskie")
    print(e)
    print(e.shape)
