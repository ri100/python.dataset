import os
from data_utils.config import pickle
from data_utils.utils.serializer import unserialize

pickle_folder = pickle['w2v_folder']
word2vec_file = os.path.join(pickle_folder, 'alle_w2v.pickle')
word2vec = unserialize(word2vec_file)
word2vec_dim = 300

