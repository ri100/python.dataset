import re, os
import numpy as np
from collections import defaultdict
from fractions import Fraction
from data_utils.config import pickle
from data_utils.utils.serializer import unserialize


class Word2Vec2:

    def __init__(self, max_chars, skip_aux=False):
        self._skip_aux = skip_aux
        self._alpha_numerics = {
            "ilość pokoi": ['pokojowy'],
            "baterie": ['komorowy'],
            "ilość szuflad": ['szufladowy'],
            "ilość biegów": ['biegowy']
        }
        self._measures = {
            "prędkość danych": ['mb/s', 'kb/s', 'gb/s', 'tb/s', 'mbps', 'kbps', 'km/s', 'm/s'],
            "prędkość": ['mph', 'km/h'],
            "spalanie": ['l/km', 'l/h'],
            "udział": ['proc', '%'],
            "czas": ['sek', 's', 'ms', 'ns', 'μs', 'nanosek', 'milisek', 'min', 'h', 'godz'],
            "ciśnienie": ['mmhg', 'pa', 'hpa', 'kpa', 'mbar', 'bar'],
            "ciężar": ['kilo', 'kg', 'kilogram', 'gr', 'gram', 'gr.', 'g', 'dg', 'dgram', 'dekagram', 't', 'ton', 'lb'],
            "objętość": ['l', 'ml', 'hl', 'fl.oz.', 'fl.oz', 'oz', "F, nF, mF, μF"],
            'pamięć': ['mb', 'kb', 'gb', 'tb'],
            "oporność": ['kohm', 'ohm', 'Ω', 'μΩ', 'kΩ'],
            "średnica": ['ø', 'fi'],
            "częstotliwość": ['μhz', 'hz', 'khz', 'ghz'],
            "moc": ['w', 'mw', 'kw', 'km', 'wat', 'watt'],
            "siła": ['nm', 'n/m', 'n', 'kn', 'mn'],
            "energia": ['μv', "v", 'kv', 'mv', 'kwh', 'mwh', 'kj', 'μa', 'µa', 'ma', 'a', 'ka', 'ah', 'mah', 'kah'],
            'kaloryczność': ['°c', 'c', 'f', '°f', 'k', '°k'],
            "jasność": ['lm', 'lx', 'nx', 'mx', 'cd·sr/m2', 'cd'],
            "głośność": ['db', 'dbi', 'dbm'],
            "powierzchnia": ['ha', 'ar', 'm2', 'mm2', 'cm2', 'ha', 'm3', 'm³', 'mm3', 'cm3', 'dm3', 'km3',
                             'm.kw.'],
            "rozmiar": ['mm', 'm', 'dm', 'km', 'cm', 'inch', '"', 'in', 'cali', 'cala', 'metrów', 'centymetrów',
                        'decymetrów',
                        'ft', 'metr.', 'metra'],
            "rozdzielczość": ['p', 'i', 'px', 'pix'],
            "obroty": ['rpm'],
            "pojemność silnika": ['tdi', 'tdci', 'jtd', 'hdi', 'tsi', 'fsi'],
            "cena": ['pln', 'zł', '$'],
            'ilość': ['szt', 'szt.', 'sztuk', 'sztuka', 'el', 'elementów', 'elem.', 'kpl', 'kompletów', 'kaps.'],
            'rok': ['r', 'rok', 'year', 'month', 'months', 'lat']
        }
        self._measure_type_dict = {m: i for i, m in enumerate(self._measures.keys())}
        self._measure_type_dim = max(self._measure_type_dict.values()) + 1
        self._measure2type = defaultdict(list)
        for t, measures_list in self._measures.items():
            _idx = self._measure_type_dict[t]
            for measure_unit in measures_list:
                self._measure2type[measure_unit].append(_idx)

        self.measures = [m for type, measures in self._measures.items() for m in measures]
        self._measure2idx = {v: k for k, v in enumerate(self.measures)}
        # print(self._measure2idx)
        # exit()
        self._max_chars = max_chars
        self._char_features_dim = 10
        self._no_of_measures = len(self.measures)
        self._measure_dim = self._no_of_measures * 3
        self._word2vec_with_pos_dim = 312
        self.aux_dim = self._max_chars + self._char_features_dim + self._measure_dim + self._measure_type_dim
        if self._skip_aux:
            self.dim = self._word2vec_with_pos_dim + 1  # for <pad>
        else:
            self.dim = self._word2vec_with_pos_dim + self.aux_dim + 1  # for <pad>

        pickle_folder = pickle['w2v_folder']
        word2vec_file = os.path.join(pickle_folder, 'pos_alle_w2v.pickle')
        self._word2vec_with_pos = unserialize(word2vec_file)
        self._char2idx = {
            "1": 101,
            "2": 102,
            "3": 103,
            "4": 104,
            "5": 105,
            "6": 106,
            "7": 107,
            "8": 108,
            "9": 109,
            "0": 110,
            ".": 10,
            ",": 11,
            "/": 12,
            "-": 14,
            "+": 15,
            "=": 20,
            " ": 30,
            "q": 40,
            "w": 41,
            "e": 42,
            "r": 43,
            "t": 44,
            "y": 45,
            "u": 46,
            "i": 47,
            "o": 48,
            "p": 49,
            "a": 50,
            "s": 51,
            "d": 52,
            "f": 53,
            "g": 54,
            "h": 55,
            "j": 56,
            "k": 57,
            "l": 58,
            "z": 60,
            "x": 61,
            "c": 62,
            "v": 63,
            "b": 64,
            "n": 65,
            "m": 66,
            "<": 71,
            ">": 72,
            "{": 75,
            "}": 76,
            "[": 78,
            "]": 79,
            "(": 82,
            ")": 83,
            "\"": 90,
            "'": 91,
            "$": 99,
        }
        self._idx2char = {v: k for k, v in self._char2idx.items()}
        self._measures_escaped = [re.escape(m) for m in self.measures]
        self._measure_reg = r'^(((\d+[.,\/]\d+|\d+)\s*x\s*)?(\d+[.,\/]\d+|\d+)\s*[x-]\s*)?(\d+[.,\/]\d+|\d+)\s*(' \
                            + "|".join(self._measures_escaped) \
                            + ')$'

        self._measure_reg = re.compile(self._measure_reg, re.IGNORECASE | re.UNICODE)
        self._serial_reg = r'^[a-z0-9\-\/\.]+$'
        self._serial_reg = re.compile(self._serial_reg, re.IGNORECASE | re.UNICODE)
        self._number_reg = r'^(?:(\d+(?:[.,]\d+)?)?\s*[x]\s*)?(\d+(?:[.,]\d+)?)\s*x\s*(\d+(?:[.,]\d+)?)|' \
                           r'(\d+(?:[.,]\d+)?\s*[-]\s*\d+(?:[.,]\d+)?)|' \
                           r'(\d+[\/]\d+)|' \
                           r'(\d+[.,]\d+|\d+)$'
        self._number_reg = re.compile(self._number_reg, re.IGNORECASE | re.UNICODE)

    @staticmethod
    def _fraction(value):
        try:
            return Fraction(value)
        except ZeroDivisionError:
            return 0.

    def char_encode(self, item):
        if not isinstance(item, str):
            item = str(item)

        char_encoded_vector = np.zeros(self._max_chars)
        measure_vector = np.zeros(self._measure_dim)
        char_feature_vector = np.zeros(self._char_features_dim)
        measure_type_vector = np.zeros(self._measure_type_dim)

        is_numeric_flag = False
        is_measure_flag = False

        # encode features
        m = self._number_reg.match(item)
        if m:
            _3d_number1, _3d_number2, _3d_number3, _range_number, _fraction_number, _number = m.groups()
            if _number:
                char_feature_vector[0] = float(_number.replace(",", "."))  # numeric
            elif _range_number:
                value1, value2 = _range_number.split('-')
                char_feature_vector[1] = 0.
                char_feature_vector[2] = float(value1.replace(",", "."))
                char_feature_vector[3] = float(value2.replace(",", "."))
            elif _fraction_number:
                value1, value2 = _fraction_number.split('/')
                char_feature_vector[1] = 0.
                char_feature_vector[2] = float(value2)
                char_feature_vector[3] = self._fraction(_fraction_number)
            elif _3d_number1 or _3d_number2 or _3d_number3:
                char_feature_vector[1] = float(_3d_number1.replace(",", ".")) if _3d_number1 else 0.
                char_feature_vector[2] = float(_3d_number2.replace(",", ".")) if _3d_number2 else 0.
                char_feature_vector[3] = float(_3d_number3.replace(",", ".")) if _3d_number3 else 0.

            # is_numeric_flag = True
        elif item.isalpha():
            char_feature_vector[7] = 1.0  # alpha
        elif self._serial_reg.match(item):
            char_feature_vector[8] = 1.0  # serial
        else:
            char_feature_vector[9] = 1.0  # other

        # encode measures

        m = self._measure_reg.match(item)
        if m:
            # print(m.groups())
            _, _, value1, value2, value3, measure = m.groups()
            value1 = self._fraction(value1.replace(",", ".")) if value1 is not None else 0
            value2 = self._fraction(value2.replace(",", ".")) if value2 is not None else 0
            value3 = self._fraction(value3.replace(",", ".")) if value3 is not None else 0

            # find position in measure vector
            # if measure in self._measure2idx:
            # ω
            if measure in self._measure2idx:
                position = self._measure2idx[measure]

                measure_vector[position * 3 + 0] = value1
                measure_vector[position * 3 + 1] = value2
                measure_vector[position * 3 + 2] = value3
            else:
                print("Unknown measure {}".format(measure))

            measure_type_idxes = self._measure2type[measure]
            for _measure_idx in measure_type_idxes:
                measure_type_vector[_measure_idx] = 1.0

            is_measure_flag = True

        if not is_measure_flag and not is_numeric_flag:

            # encode chars

            for i, c in enumerate(item[-self._max_chars:]):
                if c in self._char2idx:
                    char_encoded_vector[i] = self._char2idx[c]

        return np.concatenate([char_encoded_vector, char_feature_vector, measure_vector, measure_type_vector])

    def __getitem__(self, item):

        if self._skip_aux:
            pad = np.zeros(1)
            w2v = self._word2vec_with_pos[item]
            return np.concatenate([w2v, pad])
        elif item in self._word2vec_with_pos:
            w2v = self._word2vec_with_pos[item]
            aux = np.zeros(self.aux_dim)
        else:
            w2v = np.zeros(self._word2vec_with_pos_dim)
            aux = self.char_encode(item)

        pad = np.zeros(1)

        return np.concatenate([w2v, aux, pad])

    def __contains__(self, item):
        if self._skip_aux:
            return item in self._word2vec_with_pos
        return True

    def decode(self, vector):
        for v in vector:
            v = int(v)
            if v in self._idx2char:
                print(v, self._idx2char[v])


if __name__ == "__main__":
    w2v = Word2Vec2(max_chars=20)
    print(w2v.dim)
    # enc = w2v['100μa']
    enc = w2v['1,5m']
    print(enc)
    # enc = w2v.char_encode('pokojowy')
    # print(enc)
    # print(w2v.decode(enc))
