from annotator_dbs.storage import indices
from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros
import numpy as np


class TagEncoder(implements(EncoderInterface)):

    def __init__(self, key):
        self.key = key
        self._dim = max(indices[self.key].values()) + 1
        self._shape = (self._dim, )
        self._type = "float32"

    def encode(self, data):
        # data is tags
        if not isinstance(data, dict):
            raise ValueError("Data must be dict. {} {} given".format(type(data), data))

        tags = data
        vector = zeros(self._dim)
        is_empty = True
        if self.key in tags:
            for word in tags[self.key]:
                if word in indices[self.key]:
                    idx = indices[self.key][word]
                    vector[idx] = 1.0
                    is_empty = False
        if is_empty:
            vector[0] = 1.0

        return vector

    def decode(self, prediction):

        def __get_sentences(prediction):
            for result in prediction:
                yield np.argmax(result)

        return list(__get_sentences(prediction))

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    from annotator_enums.enums import Key
    print(indices[Key.PRODUKT1])
    w = TagEncoder(Key.ADJ)
    e = w.encode({Key.PRODUKT1: ["kajdanki"], Key.ADJ:["niebieskie"]})
    print(e)
    print(np.argmax(e))
    print(e.shape)

    e = w.encode({Key.PRODUKT1: ["niebieskie"]})
    print(e)
    print(np.argmax(e))
    print(e.shape)



