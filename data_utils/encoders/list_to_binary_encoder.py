from interface import implements
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.math.ops import zeros
import numpy as np


class ListToBinaryEncoder(implements(EncoderInterface)):

    def __init__(self, category_list):
        category_list = ['<none>'] + category_list
        self.category_2_idx = {d: i for i, d in enumerate(category_list)}
        self.idx_2_category = {i: d for i, d in enumerate(category_list)}
        self._dim = len(category_list)
        self._shape = (self._dim,)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, list):
            raise ValueError("Data must be list. {} {} given".format(type(data), data))

        vector = zeros(self._dim)
        is_empty = True
        for item in data:
            if item in self.category_2_idx:
                idx = self.category_2_idx[item]
                vector[idx] = 1.0
                is_empty = False
        if is_empty:
            vector[0] = 1.0

        return vector

    def decode(self, vector, threshold=.5):
        if not isinstance(vector, np.ndarray):
            raise ValueError("Decoded data must be list. {} {} given".format(type(vector), vector))

        for p, v in enumerate(vector):
            if v > threshold:
                yield self.idx_2_category[p]

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim


if __name__ == "__main__":
    from annotator_enums.enums import Key

    w = ListToBinaryEncoder(["abc", "dfg"])
    e = w.encode(['dfg', "abc", "_asa"])
    print(e)
    print(list(w.decode(e)))
