from interface import implements
from data_utils.encoders.dbs.word2vec2 import Word2Vec2
from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros
import numpy as np


class Word2Vec2Encoder(implements(EncoderInterface)):

    def __init__(self, max_words, skip_empty=False, skip_aux=False):
        self.skip_empty = skip_empty
        self.max_words = max_words
        self.word2vec = Word2Vec2(20, skip_aux)
        self._dim = self.word2vec.dim
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def encode(self, data):
        if not isinstance(data, str):
            raise ValueError("Word2Vec2Encoder requires data to be string.")
        result = []
        for token in data.split()[:self.max_words]:
            if token in self.word2vec:
                result.append(self.word2vec[token])
            elif not self.skip_empty:
                result.append(np.zeros(self._dim))

        try:
            return pad_2d(result, self.max_words)
        except ValueError:
            result = zeros((self.max_words, self._dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    w = Word2Vec2Encoder(16, skip_aux=True)
    e = w.encode("kajdanki erotyczne niebieskie 220µa 100ω")
    print(e)
    print(e.shape)
