from nltk import ngrams
import numpy as np

from data_utils.encoders.encoder_interface import EncoderInterface
from data_utils.encoders.padding import pad_2d
from data_utils.math.ops import zeros
from interface import implements


class NgramEncoder(implements(EncoderInterface)):

    def __init__(self, max_words, word2vectors, word2vectors_dim=300, no_of_grams=2, ngram_agg='concat'):
        self.ngram_agg = ngram_agg
        self.no_of_grams = no_of_grams
        self.max_words = max_words
        self.word2vec = word2vectors
        self.word2vectors_dim = word2vectors_dim
        if self.ngram_agg == 'concat':
            self._dim = word2vectors_dim * self.no_of_grams
        elif self.ngram_agg == 'sum':
            self._dim = word2vectors_dim
        else:
            raise ValueError('Incorrect ngram_agg, available [sum, concat] given {}'.format(self.ngram_agg))
        self._shape = (self.max_words, self._dim)
        self._type = "float32"

    def _ngram_tokens(self, tokens, no_of_grams):
        result = []
        for gram in ngrams(tokens, no_of_grams):
            gram_concat = []
            for token in gram:
                if token in self.word2vec:
                    gram_concat.append(self.word2vec[token])
                else:
                    gram_concat.append(zeros(self.word2vectors_dim))

            if self.ngram_agg == 'concat':
                vec = np.concatenate(tuple(gram_concat))
            elif self.ngram_agg == 'sum':
                vec = np.sum(gram_concat, axis=0)
            else:
                raise ValueError('Incorrect ngram_agg, available [sum, concat] given {}'.format(self.ngram_agg))

            vec_dim = vec.shape[-1]

            if self._dim > vec_dim:
                z = zeros(self._dim)
                z[:vec_dim] = vec
                vec = z

            result.append(vec)
        return result

    def encode(self, data):
        tokens = data.split()
        tokens = [t for t in tokens if t in self.word2vec]
        no_of_grams = min(self.no_of_grams, len(tokens))

        result = self._ngram_tokens(tokens, no_of_grams)

        try:
            return pad_2d(result, self.max_words)
        except ValueError:
            result = zeros((self.max_words, self._dim))
            result[..., -1] = 1.0
            return result

    def shape(self):
        return self._shape

    def type(self):
        return self._type

    def dim(self):
        return self._dim

    def __contains__(self, token):
        return token in self.word2vec


if __name__ == "__main__":
    from data_utils.encoders.dbs.word2vec import word2vec

    w = NgramEncoder(6, word2vec, ngram_agg='concat')
    e = w.encode("")
    print(e)
    print(e.shape)
