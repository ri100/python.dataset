import pickle


def unserialize(name):
    with open(name, 'rb') as f:
        return pickle.load(f)


def serialize(obj, name):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
