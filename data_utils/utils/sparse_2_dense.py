from data_utils.math.ops import zeros


class SparseToDense:

    def __init__(self, data, dim):
        self.data = data
        self.dim = dim

    def __getitem__(self, item):
        return self.to_vec(item)

    def to_vec(self, word):
        vec = zeros(self.dim)
        if word in self.data:
            for idx in self.data[word]:
                vec[idx] = 1.0
        else:
            vec[-2] = 1.0

        return vec
